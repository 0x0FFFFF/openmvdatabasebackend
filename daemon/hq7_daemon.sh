## /home/java/back/hq7_daemon.sh
## chmod 755 hq7_daemon.sh
## groupadd -r web
## useradd -r -g web -d /home/java/back -s /sbin/nologin web
## chown web:web -R /home/java/back

#!/bin/sh

if [ "x$BACKEND_HOME" = "x" ]; then
    BACKEND_HOME=/home/java/back/target
    JAR_NAME=database-0.0.1-SNAPSHOT.jar
fi

echo 'Starting backend...'
java -Xdebug -Xrunjdwp:server=y,transport=dt_socket,suspend=n -jar $BACKEND_HOME/$JAR_NAME

fi
