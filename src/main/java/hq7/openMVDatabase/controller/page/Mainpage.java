package hq7.openMVDatabase.controller.page;

import hq7.openMVDatabase.model.title.Title;
import hq7.openMVDatabase.repository.page.PageRepository;
import hq7.openMVDatabase.repository.title.TitleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/page")
@CrossOrigin
public class Mainpage {

    private PageRepository pageRepository;
    private TitleRepository titleRepository;

    @Autowired
    public Mainpage(PageRepository pageRepository, TitleRepository titleRepository){
        this.pageRepository = pageRepository;
        this.titleRepository = titleRepository;
    }

    /**
     * Главная страница
     * @return Бэкраунд, трейлер, ссылки и тд
     */
    @GetMapping(value = "/main", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<hq7.openMVDatabase.model.page.Mainpage> getAll(){
        return pageRepository.findAll();
    }

    /**
     * Главная страница
     * @return Популярный контент
     */
    @GetMapping(value = "/popular")
    public List<Title> getAllByPopular(){
        return titleRepository.findAllByOrderByNoOfTimesAccessedDesc();
    }
}
