package hq7.openMVDatabase.controller.title;

import hq7.openMVDatabase.model.title.Title;
import hq7.openMVDatabase.repository.title.TitleRepository;
import hq7.openMVDatabase.service.title.TitleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/titles")
@CrossOrigin
public class TitleController {

    private TitleRepository titleRepository;
    private TitleService titleService;

    @Autowired
    public TitleController(TitleRepository titleRepository, TitleService titleService){
        this.titleRepository = titleRepository;
        this.titleService = titleService;
    }

    /**
     * Залогиненый пользователь
     * @return Все titlеs из бд
     */
    @PreAuthorize(value = "hasAnyAuthority('USER', 'ADMIN')")
    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Title> getAll(){
        return titleRepository.findAll();
    }

    /**
     * Залогиненый пользователь
     * @return titlеs
     */
    @PreAuthorize(value = "hasAnyAuthority('USER', 'ADMIN')")
    @GetMapping(value = "/all/byAccesses")
    public List<Title> getAllByNoOfAccesses(){
        return titleRepository.findAllByOrderByNoOfTimesAccessedDesc();
    }

    /**
     * Залогиненый пользователь
     * @return Все titlеs из бд по выбранной категории(стране)
     */
    @PreAuthorize(value = "hasAnyAuthority('USER', 'ADMIN')")
    @GetMapping(value = "/all/byCategory/{theCategory}")
    public List<Title> getAllByCategory(@PathVariable String theCategory){
        return titleRepository.findAllByContentCountry(titleService.findCategory(theCategory));
    }

    /**
     * Залогиненый пользователь
     * @return Реализация поиска
     */
    @PreAuthorize(value = "hasAnyAuthority('USER', 'ADMIN')")
    @GetMapping(value = "/all/byName/{term}")
    public List<Title> getAllByName(@PathVariable String term){
        return titleRepository.findAllByTitleNameContainingIgnoreCase(term);
    }

    /**
     * Залогиненый пользователь
     * @return Создание нового материала
     */
    @PreAuthorize(value = "hasAnyAuthority('USER', 'ADMIN')")
    @PostMapping(value = "/create")
    public Title postTitle(@RequestBody Title title){
        titleService.newTitle(title);
        return title;
    }

    /**
     * Залогиненый пользователь
     * @return Удаление существующего материала
     */
    @PreAuthorize(value = "hasAnyAuthority('USER', 'ADMIN')")
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity deleteTitle(@PathVariable long id){
        if (titleRepository.findById(id).isPresent()) {
            titleRepository.deleteById(id);
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    /**
     * Залогиненый пользователь
     * @return Получение данных titlе
     */
    @PreAuthorize(value = "hasAnyAuthority('USER', 'ADMIN')")
    @GetMapping(value = "/title/{id}")
    public ResponseEntity<Title> getTitle(@PathVariable long id){
        if (titleRepository.findById(id).isPresent())
            return new ResponseEntity<>(titleService.getTitle(id), HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Залогиненый пользователь
     * @return Редактирование titlе
     */
    @PreAuthorize(value = "hasAnyAuthority('USER', 'ADMIN')")
    @PostMapping(value = "/update/{id}")
    public Title updateTitle(@RequestBody Title title, @PathVariable long id){
        return titleService.updateTitle(title, id);
    }
}
