package hq7.openMVDatabase.controller.user;

import hq7.openMVDatabase.model.user.User;
import hq7.openMVDatabase.repository.user.UserRepository;
import hq7.openMVDatabase.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Контроллер пользователей
 */

@RestController
@RequestMapping(value = "/users")
@CrossOrigin
public class Users {

    private UserRepository userRepository;
    private UserService userService;

    @Autowired
    public Users(UserRepository userRepository, UserService userService){
        this.userRepository = userRepository;
        this.userService = userService;
    }

    /**
     * Залогиненый пользователь
     * Информация о зарегистрированных пользователях
     * @return список зарегистрированных пользователей
     */
    @PreAuthorize(value = "hasAnyAuthority('ADMIN')")
    @GetMapping(value = "/all")
    public List<User> getAllUsers(){
        return userRepository.findAllByOrderByIdAsc();
    }

//    @GetMapping(value = "/info/{id}")
//    public Optional<User> getUserInfo(@PathVariable long id){
//        return userRepository.findById(id);
//    }

    /**
     * Залогиненый пользователь
     * Информация о конкретном зарегистрированном пользователе
     * @return кастомный ответ с информацией о конкретном пользователе
     * @param id - ид пользователя
     */
    @PreAuthorize(value = "hasAnyAuthority('ADMIN')")
    @GetMapping(value = "/info/{id}")
    public ResponseEntity<User> getUser(@PathVariable long id){
        if (userRepository.findById(id).isPresent())
            return new ResponseEntity<>(userService.getUser(id), HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Залогиненый пользователь
     * Редактирование роли пользователя
     * @return обновленная иформация о пользователе
     * @param id - ид пользователя
     * @param role - нужная роль пользователя (admin/user)
     */
    @PreAuthorize(value = "hasAnyAuthority('ADMIN')")
    @PostMapping(value = "/set/role/{id}/{role}")
    public User updateRole(@PathVariable long id, @PathVariable String role){
        return userService.updateRole(id, role);
    }

}
