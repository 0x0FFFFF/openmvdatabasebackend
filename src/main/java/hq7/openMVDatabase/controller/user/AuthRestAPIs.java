package hq7.openMVDatabase.controller.user;

import hq7.openMVDatabase.message.request.LoginForm;
import hq7.openMVDatabase.message.request.SignUpForm;
import hq7.openMVDatabase.message.response.JwtResponse;
import hq7.openMVDatabase.message.response.ResponseMessage;
import hq7.openMVDatabase.model.user.Role;
import hq7.openMVDatabase.model.user.RoleName;
import hq7.openMVDatabase.model.user.User;
import hq7.openMVDatabase.repository.user.RoleRepository;
import hq7.openMVDatabase.repository.user.UserRepository;
import hq7.openMVDatabase.security.jwt.JwtProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Контроллер авторизации
 */

@RestController
@RequestMapping("/api/auth")
@CrossOrigin(origins = "*", maxAge = 3600)
public class AuthRestAPIs {

    private AuthenticationManager authenticationManager;

    private UserRepository userRepository;

    private RoleRepository roleRepository;

    private PasswordEncoder encoder;

    private JwtProvider jwtProvider;

    @Autowired
    public AuthRestAPIs(AuthenticationManager authenticationManager, UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder encoder, JwtProvider jwtProvider){
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
        this.jwtProvider = jwtProvider;
    }

    /**
     * Вход пользователя
     * @return статус операции
     */
    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginForm loginForm) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                loginForm.getUsername(), loginForm.getPassword()
        ));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtProvider.generateJwtToken(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities()));
    }

    /**
     * Регистрация пользователя
     * @return статус операции
     */
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpForm signUpForm) {

        // Проверим БД на существавание никнейма
        if(userRepository.existsByUsername(signUpForm.getUsername())){
            return new ResponseEntity<>(new ResponseMessage("Такой пользователь уже существует!"), HttpStatus.BAD_REQUEST);
        }

        // Проверим БД на существавание почты
        if(userRepository.existsByEmail(signUpForm.getEmail())){
            return new ResponseEntity<>(new ResponseMessage("Такой email уже существует!"), HttpStatus.BAD_REQUEST);
        }

        User user = new User(signUpForm.getName(), signUpForm.getUsername(), signUpForm.getEmail(), encoder.encode(signUpForm.getPassword()));

        Set<String> strRoles = signUpForm.getRole();
        Set<Role> roles = new HashSet<>();

        strRoles.forEach(role -> {
            if (role.equals("admin")) {
                Role adminRole = roleRepository.findByRoleName(RoleName.ADMIN)
                        .orElseThrow(() -> new RuntimeException("Ошибка: Нет прав."));
                roles.add(adminRole);
            } else {
                Role userRole = roleRepository.findByRoleName(RoleName.USER)
                        .orElseThrow(() -> new RuntimeException("Ошибка: Нет прав."));
                roles.add(userRole);
            }
        });

        user.setRoles(roles);
        userRepository.save(user);

        return new ResponseEntity<>(new ResponseMessage("Успешная регистрация!"), HttpStatus.OK);
    }

}
