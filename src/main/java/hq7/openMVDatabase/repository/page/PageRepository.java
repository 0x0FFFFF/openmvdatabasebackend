package hq7.openMVDatabase.repository.page;

import hq7.openMVDatabase.model.page.Mainpage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PageRepository extends JpaRepository<Mainpage, Long> {

    List<Mainpage> findAll();

    /**
     * Пример нативного SQL. Не, ну это чисто для того, чтобы не только голые findBy* были
     * @return 10 популярных татйлов
     */
    @Query(value = "SELECT * FROM 'titles' ORDER BY 'no_of_times_accessed' DESC COUNT 10", nativeQuery = true)
    List<Mainpage> getAllByPopular();
}
