package hq7.openMVDatabase.repository.title;

import hq7.openMVDatabase.model.title.Content;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContentRepository extends JpaRepository<Content, Long> {
}
