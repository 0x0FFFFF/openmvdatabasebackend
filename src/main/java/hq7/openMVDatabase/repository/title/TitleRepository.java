package hq7.openMVDatabase.repository.title;

import hq7.openMVDatabase.model.title.Title;
import hq7.openMVDatabase.model.title.ContentCountry;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TitleRepository extends JpaRepository<Title, Long> {
    /** @return titlеs по стране
     */
    List<Title> findAllByContentCountry(ContentCountry contentCountry);
    /** @return titlеs с игнорированием регистра
     */
    List<Title> findAllByTitleNameContainingIgnoreCase(String titleName);
    /** @return titlеs по популярности
     */
    List<Title> findAllByOrderByNoOfTimesAccessedDesc();
}
