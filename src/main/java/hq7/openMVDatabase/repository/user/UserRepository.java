package hq7.openMVDatabase.repository.user;

import hq7.openMVDatabase.model.user.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    /** @return поиск пользователя
     * @param username
     */
    Optional<User> findByUsername(String username);
    /** @return поиск пользователей
     *
     */
    List<User> findAllByOrderByIdAsc();
    /** @return проверка существования по имени
     * @param username
     */
    Boolean existsByUsername(String username);
    /** @return проверка существования по почте
     * @param email
     */
    Boolean existsByEmail(String email);
}
