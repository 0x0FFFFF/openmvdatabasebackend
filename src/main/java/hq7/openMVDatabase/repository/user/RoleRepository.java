package hq7.openMVDatabase.repository.user;

import hq7.openMVDatabase.model.user.Role;
import hq7.openMVDatabase.model.user.RoleName;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {

    /** @return поиск по имени роли
     * @param roleName
     */
    Optional<Role> findByRoleName(RoleName roleName);
}
