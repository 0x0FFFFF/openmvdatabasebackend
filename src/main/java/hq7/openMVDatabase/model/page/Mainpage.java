package hq7.openMVDatabase.model.page;

import hq7.openMVDatabase.model.title.Content;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/** Модель главной страницы
 */

@Entity
@Table(name = "mainpage")
public class Mainpage implements Serializable {

    @Id
    @GeneratedValue
    private int id;

    @NotBlank
    @Column(columnDefinition = "VARCHAR(255)")
    private String background;

    @NotBlank
    @Column(columnDefinition = "VARCHAR(255)")
    private String top_title;

    @NotBlank
    @Column(columnDefinition = "VARCHAR(255)")
    private String bottom_title;

    @NotBlank
    @Column(columnDefinition = "VARCHAR(255)")
    private String trailer_url;

    @NotBlank
    @Column(columnDefinition = "VARCHAR(255)")
    private String link_to_watch;

    @NotBlank
    @Column(columnDefinition = "VARCHAR(255)")
    private String text_to_watch;

    @NotBlank
    @Column(columnDefinition = "VARCHAR(255)")
    private String poster;

    @NotBlank
    @Column(columnDefinition = "VARCHAR(30)")
    private String date_text;

    @NotBlank
    @Column(columnDefinition = "VARCHAR(20)")
    private String time;

    @NotBlank
    @Column(columnDefinition = "LONGTEXT")
    private String text;

//    @JoinTable(name = "titles", joinColumns = @JoinColumn(name = "title_id"), inverseJoinColumns = @JoinColumn(name = "id"))
//    private Set<Content> contents = new HashSet<>();
//
//    public Set<Content> getContents() {
//        return contents;
//    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getTop_title() {
        return top_title;
    }

    public void setTop_title(String top_title) {
        this.top_title = top_title;
    }

    public String getBottom_title() {
        return bottom_title;
    }

    public void setBottom_title(String bottom_title) {
        this.bottom_title = bottom_title;
    }

    public String getTrailer_url() {
        return trailer_url;
    }

    public void setTrailer_url(String trailer_url) {
        this.trailer_url = trailer_url;
    }

    public String getLink_to_watch() {
        return link_to_watch;
    }

    public void setLink_to_watch(String link_to_watch) {
        this.link_to_watch = link_to_watch;
    }

    public String getText_to_watch() {
        return text_to_watch;
    }

    public void setText_to_watch(String text_to_watch) {
        this.text_to_watch = text_to_watch;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getDate_text() {
        return date_text;
    }

    public void setDate_text(String date_text) {
        this.date_text = date_text;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
