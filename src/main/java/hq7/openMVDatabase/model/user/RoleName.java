package hq7.openMVDatabase.model.user;

/** Модель пользователя
 * ENUM ролей
 */

public enum RoleName {
    ADMIN, USER
}
