package hq7.openMVDatabase.model.title;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/** Модель листа наград
 */

@Entity
@Table(name = "awards")
public class Content implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long awardId;

    @NotBlank
    private String awardName;

    private String quantity;

    public Content(){}

    public Content(String awardName, String quantity) {
        this.awardName = awardName;
        this.quantity = quantity;
    }

    public long getAwardId() {
        return awardId;
    }

    public void setAwardId(long awardId) {
        this.awardId = awardId;
    }

    public String getAwardName() {
        return awardName;
    }

    public void setAwardName(String awardName) {
        this.awardName = awardName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
