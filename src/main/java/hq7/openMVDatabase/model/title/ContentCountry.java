package hq7.openMVDatabase.model.title;

/** ENUM стран
 */

public enum ContentCountry {
    KR, JP, CN
}
