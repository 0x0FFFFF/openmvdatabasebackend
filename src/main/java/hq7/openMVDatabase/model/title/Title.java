package hq7.openMVDatabase.model.title;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/** Модель titlеа
 */

@Entity
@Table(name = "titles")
public class Title implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "title_id")
    private long titleId;

    @NotBlank
    @Column(columnDefinition = "VARCHAR(255)")
    private String titleName;

    @Enumerated
    @Column(columnDefinition = "smallint")
    private ContentCountry contentCountry;

    @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "title_id")
    private List<Content> awardsList = new ArrayList<>();

    @NotBlank
    @Column(columnDefinition = "LONGTEXT")
    private String instructions;

    @Column(columnDefinition = "LONGTEXT")
    private String suggestions;

    @Column(columnDefinition = "VARCHAR(255)")
    private String image;

    private Date lastAccessed;

    private long noOfTimesAccessed;

    public Title(){}

    public Title(String titleName, ContentCountry contentCountry, String instructions, String suggestions){
        this.titleName = titleName;
        this.contentCountry = contentCountry;
        this.instructions = instructions;
        this.suggestions = suggestions;
    }

    public long getTitleId() {
        return titleId;
    }

    public void setTitleId(long titleId) {
        this.titleId = titleId;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    public ContentCountry getContentCountry() {
        return contentCountry;
    }

    public void setContentCountry(ContentCountry contentCountry) {
        this.contentCountry = contentCountry;
    }

    public List<Content> getAwardsList() {
        return awardsList;
    }

    public void setAwardsList(List<Content> awardsList) {
        this.awardsList.clear();
        if(awardsList != null)
            this.awardsList.addAll(awardsList);
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(String suggestions) {
        this.suggestions = suggestions;
    }

    public Date getLastAccessed() {
        return lastAccessed;
    }

    public void setLastAccessed(Date lastAccessed) {
        this.lastAccessed = lastAccessed;
    }

    public long getNoOfTimesAccessed() {
        return noOfTimesAccessed;
    }

    public void setNoOfTimesAccessed(long noOfTimesAccessed) {
        this.noOfTimesAccessed = noOfTimesAccessed;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


}
