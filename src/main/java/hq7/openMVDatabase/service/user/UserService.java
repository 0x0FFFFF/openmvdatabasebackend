package hq7.openMVDatabase.service.user;

import hq7.openMVDatabase.model.title.Title;
import hq7.openMVDatabase.model.user.Role;
import hq7.openMVDatabase.model.user.RoleName;
import hq7.openMVDatabase.model.user.User;
import hq7.openMVDatabase.repository.user.RoleRepository;
import hq7.openMVDatabase.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class UserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;

    @Autowired
    public UserService(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    /** Редактирование прав пользователя
     * @return обеолвенная информация о пользователе
     */
    public User getUser(long id) {
        User user = userRepository.findById(id).orElse(null);
        return user;
    }

    /** Редактирование прав пользователя
     * @return обеолвенная информация о пользователе
     */
    public User updateRole(long id, String role) {
        User userToSave = userRepository.findById(id).orElse(null);
        Set<Role> roles = new HashSet<>();

        if (role.equals("admin")) {
            Role adminRole = roleRepository.findByRoleName(RoleName.ADMIN)
                    .orElseThrow(() -> new RuntimeException("Ошибка: Нет прав."));
            roles.add(adminRole);
        } else {
            Role userRole = roleRepository.findByRoleName(RoleName.USER)
                    .orElseThrow(() -> new RuntimeException("Ошибка: Нет прав."));
            roles.add(userRole);
        }

        if (userToSave != null) {
            userToSave.setRoles(roles);
            userRepository.save(userToSave);

            return userToSave;
        }
        return null;
    }


}

