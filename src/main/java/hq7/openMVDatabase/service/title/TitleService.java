package hq7.openMVDatabase.service.title;

import hq7.openMVDatabase.model.title.Title;
import hq7.openMVDatabase.model.title.ContentCountry;
import hq7.openMVDatabase.repository.title.TitleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class TitleService {

    private TitleRepository titleRepository;

    @Autowired
    public TitleService(TitleRepository titleRepository) {
        this.titleRepository = titleRepository;
    }

    /**
     * Маппинг категорий(стран)
     */
    public ContentCountry findCategory(String category) {
        ContentCountry contentCountry = ContentCountry.KR;
        if(category.equals("1"))
            contentCountry = ContentCountry.JP;
        if(category.equals("2"))
            contentCountry = ContentCountry.CN;

        return contentCountry;
    }

    /**
     * Залогиненый пользователь
     * Создать материал
     */
    public void newTitle(Title title) {
        title.setLastAccessed(new Date());
        titleRepository.save(title);
    }

    /**
     * Залогиненый пользователь
     * Получить материал
     */
    public Title getTitle(long id) {
        Title titleToSave = titleRepository.findById(id).orElse(null);
        Title titleToReturn = new Title();

        if (titleToSave != null) {
            titleToReturn.setTitleId(titleToSave.getTitleId());
            copyTitle(titleToReturn, titleToSave);
            titleToSave.setLastAccessed(new Date());
            titleToSave.setNoOfTimesAccessed(titleToReturn.getNoOfTimesAccessed() + 1);

            titleRepository.save(titleToSave);

            return titleToReturn;
        }
        return null;
    }

    /**
     * Залогиненый пользователь
     * Редактировать матриал
     */
    public Title updateTitle(Title title, long id) {
        Optional<Title> titleToCheck = titleRepository.findById(id);

        if (titleToCheck.isPresent()) {
            Title titleToSave = titleToCheck.get();
            copyTitle(titleToSave, title);

            titleRepository.save(titleToSave);
            return titleToSave;
        } else {
            return null;
        }
    }

    /**
     * Махарайка для редактирования
     */
    private void copyTitle(Title title1, Title title2) {
        title1.setTitleName(title2.getTitleName());
        title1.setContentCountry(title2.getContentCountry());
        title1.setImage(title2.getImage());
        title1.setAwardsList(title2.getAwardsList());
        title1.setInstructions(title2.getInstructions());
        title1.setSuggestions(title2.getSuggestions());
        title1.setLastAccessed(title2.getLastAccessed());
        title1.setNoOfTimesAccessed(title2.getNoOfTimesAccessed());
    }
}
