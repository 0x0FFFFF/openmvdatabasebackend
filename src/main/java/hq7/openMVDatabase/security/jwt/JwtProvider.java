package hq7.openMVDatabase.security.jwt;

import hq7.openMVDatabase.security.services.UserPrinciple;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtProvider.class);

    /** Тащим ключ шифрования (секрет) из глобалки
     */
    @Value("${database.var.jwtSecret}")
    private String jwtSecret;

    /** Тащим время жизни из глобалки
     */
    @Value("${database.var.jwtExpiration}")
    private int jwtExpiration;

    // Билдим
    /** Билдим
     */
    public String generateJwtToken(Authentication authentication){
        UserPrinciple userPrinciple = (UserPrinciple) authentication.getPrincipal();

        return Jwts.builder()
                .setSubject(userPrinciple.getUsername()) // Имя пользователя
                .setAudience(userPrinciple.getAuthorities().toString()) // Роль пользователя
                .setIssuedAt(new Date()) // Дата создания токена
                .setExpiration(new Date((new Date()).getTime() + jwtExpiration*1000)) // Дата истечения токена
                .signWith(SignatureAlgorithm.HS512, jwtSecret) // Алго токена
                .compact();
    }

    // Проверяем
    /** Проверяем
     */
    public boolean validateJwtToken(String authToken){
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature -> Message: {}", e);
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token -> Message: {}", e);
        } catch (ExpiredJwtException e) {
            logger.error("Expired JWT token -> Message: {}", e);
        } catch (UnsupportedJwtException e) {
            logger.error("Unsupported JWT token -> Message: {}", e);
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty -> Message: {}", e);
        }

        return false;
    }

    // Парсим имя
    /** Парсим имя
     */
    public String getUserNameFromJwtToken(String token){
        return Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody().getSubject();
    }
}
